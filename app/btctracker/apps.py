from django.apps import AppConfig


class BtctrackerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'btctracker'
