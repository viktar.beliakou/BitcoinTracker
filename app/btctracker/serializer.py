from rest_framework import serializers
from .models import TradeRegistry

class ModelSerializer(serializers.ModelSerializer):
    user_id = serializers.DecimalField(max_digits=16 , decimal_places=6)
    trade_type = serializers.CharField(max_length=3)
    price = serializers.DecimalField(max_digits=6, decimal_places=1)
    trade_size = serializers.DecimalField(max_digits=6, decimal_places=1)
    timestamp = serializers.DecimalField(max_digits=16 , decimal_places=6)

    class Meta:
        model = TradeRegistry
        fields = ('__all__')