from django.db import models

# Create your models here.
class TradeRegistry(models.Model):

    user_id = models.DecimalField(max_digits=16 , decimal_places=6)
    trade_type = models.CharField(max_length=3)
    price = models.DecimalField(max_digits=6, decimal_places=1)
    trade_size = models.DecimalField(max_digits=6, decimal_places=1)
    timestamp = models.DecimalField(max_digits=16 , decimal_places=6)