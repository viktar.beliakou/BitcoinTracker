from django.contrib import admin
from .models import TradeRegistry

class RegistryAdmin(admin.ModelAdmin):
    list_display = (
        'user_id',
        'trade_type',
        'price',
        'trade_size',
        'timestamp'
    )

admin.site.register(TradeRegistry, RegistryAdmin)