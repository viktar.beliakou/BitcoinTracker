import json 

from channels.generic.websocket import WebsocketConsumer 

from .FTXapi import FtxWebsocketClient

class WSConsumer(WebsocketConsumer):

    def __init__(self):
        super().__init__()
        self._ws_client = FtxWebsocketClient()
        self._ticker = None

    def connect(self):
        self.accept()

        while True:
            self._update_ticker()
            self.send(
                json.dumps(
                   self._ticker
                )   
            )

    def _update_ticker(self):
        self._ticker = self._ws_client.get_ticker(market='BTC/USD')
        
