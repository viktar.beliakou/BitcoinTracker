from django.urls import path 
from .consumer import WSConsumer 

ws_urlpatterns = [
    path('ws/ftx/', WSConsumer.as_asgi()),
]

