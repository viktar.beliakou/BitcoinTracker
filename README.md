# Bitcoin Exchange

## Trade registration

The trades will either be confirmed or rejected by the backend trade processing engine, depending
on whether the price the user clicked is old or not. The trade requests should be logged into a database, and a confirmation or error message sent back to the user and displayed on the GUI.

When clicking either the BUY or SELL boxes, the GUI will send a JSON trade request to the backend
using a REST API.

Fields to include in a database:

* User ID - timestamp
* Buy/Sell - Ask/Bid - (not mentioned in specs, however, included)
* Price that was last shown - "ask/bid"
* User selected size - less than "askSize"
* Timestamp of the ticker - "time"

## Rules around registration

If the price is too old (timestamp > 2 seconds old), or the user selected size is too large for the
available quoted price (e.g. bidSize is 0.08 as above, and user wants to sell 1 BTC) then the trade
request is rejected – the user will be sent a 4xx HTTP status code in the response, and the response
body will explain the reason (stale price – time too old, or unavailable size – bid/askSize too small).

If the trade request can be accepted by the backend, then a confirmation of trade is sent to the user,
with some trade ID (random string).

## Superuser details 

* vvb@gmail.com
* vvb123654
